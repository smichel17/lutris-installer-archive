{
  "id": 29408,
  "game_id": 41760,
  "game_slug": "ascension-wow",
  "name": "Ascension WoW",
  "year": 2004,
  "user": "LutrisTest",
  "runner": "linux",
  "slug": "ascension-wow-linux-launcher",
  "version": "Linux Launcher",
  "description": "Installer for the Linux Launcher of Project Ascension. The installer creates a small update/start script to automatically update the launcher if a new version is published.",
  "notes": "**INSTALL THE DEPENDENCIES FIRST**\r\n1.) Update your system first.\r\n2.) Install the dependencies: wine, winetricks, mono or mono-complete, grep, wget and zenity\r\n3.) Optional: Instructions for Vulkan support: https://github.com/lutris/lutris/wiki/Installing-drivers\r\n4.) If you have no Lutris wine version installed, please do so before you start the installation. This is to prevent an error that Lutris can't find a wine executable when the wine runner is only used indirectly in the installation script. Just go to the \"Manage Versions\" dialog of the wine runner and install the latest version.\r\n\r\n**CONFIGURE LUTRIS (RECOMMENDED)**\r\nTo minimize error sources it is recommended to change some of the wine runner settings before installing the game. After the installation you can revert these changes. To change some of these settings you need to have enabled the \"show advanced options\" checkbox.\r\n1.) Enable the setting \"use system winetricks\" to minimize winetricks errors during the installation because your system version is most likely newer than the Lutris version.\r\n\r\nNeed to go into game directory and run Appimage file to make game work login and install.",
  "credits": "Myself",
  "created_at": "2021-09-11T17:14:49.390000Z",
  "updated_at": "2025-02-10T02:01:44.930836Z",
  "draft": false,
  "published": true,
  "published_by": 1,
  "rating": "",
  "is_playable": null,
  "steamid": null,
  "gogid": null,
  "gogslug": "",
  "humbleid": "",
  "humblestoreid": "",
  "humblestoreid_real": "",
  "script": {
    "files": [
      {
        "launcher": {
          "filename": "ascension-launcher.AppImage",
          "url": "https://api.ascension.gg/api/bootstrap/launcher/latest?unix"
        }
      },
      {
        "mono": {
          "filename": "wine-mono.msi",
          "url": "https://dl.winehq.org/wine/wine-mono/9.1.0/wine-mono-9.1.0-x86.msi"
        }
      }
    ],
    "game": {
      "exe": "$GAMEDIR/updateAndRun.sh",
      "working_dir": "$GAMEDIR"
    },
    "installer": [
      {
        "input_menu": {
          "description": "If you installed Project Ascension before, please remove the folder or symlink ~/.config/projectascension.",
          "options": [
            {
              "waiting": "Click here to continue"
            },
            {
              "continue": "I have removed previous installations, continue with the installation"
            }
          ],
          "preselect": "waiting"
        }
      },
      {
        "execute": {
          "command": "ln -s $GAMEDIR $HOME/.config/projectascension",
          "description": "Create symlink to game folder"
        }
      },
      {
        "move": {
          "description": "Copy game launcher to game folder",
          "dst": "$GAMEDIR/ascension-launcher.AppImage",
          "src": "launcher"
        }
      },
      {
        "chmodx": "$GAMEDIR/ascension-launcher.AppImage"
      },
      {
        "write_file": {
          "content": "#!/bin/bash\nsource currentLauncherVersion\nnewVersion=$(curl https://api.ascension.gg/api/bootstrap/launcher | grep -oP '\"version\":\"\\K.+?(?=\")' || echo \"NO_CONNECTION\")\nif [[ -z \"$version\" || ($version != $newVersion && \"NO_CONNECTION\" != $newVersion) ]];\nthen\necho \"version=$newVersion\" > currentLauncherVersion\nrm ascension-launcher.AppImage_old\nmv ascension-launcher.AppImage ascension-launcher.AppImage_old\nwget -O ascension-launcher.AppImage --progress=dot \"https://api.ascension.gg/api/bootstrap/launcher/latest?unix\" 2>&1 | sed 's/^.*\\s\\+\\(.\\+\\)%\\s\\+\\(.\\+\\)\\s\\+\\(.\\+\\)\\s*$/\\1\\n# Downloading at \\2\\/s ETA \\3/' | zenity --title=\"Update Launcher\" --width=300 --progress --percentage=0 --no-cancel --auto-close\nchmod u+x ascension-launcher.AppImage\nfi\n./ascension-launcher.AppImage",
          "description": "Create update launcher script",
          "file": "$GAMEDIR/updateAndRun.sh"
        }
      },
      {
        "chmodx": "$GAMEDIR/updateAndRun.sh"
      },
      {
        "execute": {
          "command": "echo \"version=$(curl https://api.ascension.gg/api/bootstrap/launcher | grep -oP '\"version\":\"\\K.+?(?=\")')\" > $GAMEDIR/currentLauncherVersion",
          "description": "Fetch current launcher version"
        }
      },
      {
        "task": {
          "arch": "win32",
          "description": "Create 32bit Wine prefix",
          "name": "wine.create_prefix",
          "prefix": "$GAMEDIR/WoW"
        }
      },
      {
        "task": {
          "app": "remove_mono",
          "arch": "win32",
          "description": "Remove preinstalled mono in Wine prefix",
          "name": "wine.winetricks",
          "prefix": "$GAMEDIR/WoW"
        }
      },
      {
        "task": {
          "arch": "win32",
          "description": "Install wine mono",
          "executable": "mono",
          "name": "wine.wineexec",
          "prefix": "$GAMEDIR/WoW"
        }
      },
      {
        "task": {
          "app": "-f win10 ie8 corefonts dotnet48 vcrun2015",
          "arch": "win32",
          "description": "Install win10, ie8, corefonts, dotnet48 und vcrun2015 with winetricks",
          "name": "wine.winetricks",
          "prefix": "$GAMEDIR/WoW"
        }
      },
      {
        "input_menu": {
          "description": "Do you want to install DXVK?",
          "id": "dxvk",
          "options": [
            {
              "dxvk": "Yes"
            },
            {
              "good": "No"
            }
          ],
          "preselect": "good"
        }
      },
      {
        "task": {
          "app": "$INPUT_dxvk",
          "arch": "win32",
          "description": "Install DXVK with winetricks",
          "name": "wine.winetricks",
          "prefix": "$GAMEDIR/WoW"
        }
      }
    ],
    "require-binaries": "wine, winetricks, grep, wget, zenity, mono | mono-complete"
  },
  "content": "files:\n- launcher:\n    filename: ascension-launcher.AppImage\n    url: https://api.ascension.gg/api/bootstrap/launcher/latest?unix\n- mono:\n    filename: wine-mono.msi\n    url: https://dl.winehq.org/wine/wine-mono/9.1.0/wine-mono-9.1.0-x86.msi\ngame:\n  exe: $GAMEDIR/updateAndRun.sh\n  working_dir: $GAMEDIR\ninstaller:\n- input_menu:\n    description: If you installed Project Ascension before, please remove the folder\n      or symlink ~/.config/projectascension.\n    options:\n    - waiting: Click here to continue\n    - continue: I have removed previous installations, continue with the installation\n    preselect: waiting\n- execute:\n    command: ln -s $GAMEDIR $HOME/.config/projectascension\n    description: Create symlink to game folder\n- move:\n    description: Copy game launcher to game folder\n    dst: $GAMEDIR/ascension-launcher.AppImage\n    src: launcher\n- chmodx: $GAMEDIR/ascension-launcher.AppImage\n- write_file:\n    content: '#!/bin/bash\n\n      source currentLauncherVersion\n\n      newVersion=$(curl https://api.ascension.gg/api/bootstrap/launcher | grep -oP\n      ''\"version\":\"\\K.+?(?=\")'' || echo \"NO_CONNECTION\")\n\n      if [[ -z \"$version\" || ($version != $newVersion && \"NO_CONNECTION\" != $newVersion)\n      ]];\n\n      then\n\n      echo \"version=$newVersion\" > currentLauncherVersion\n\n      rm ascension-launcher.AppImage_old\n\n      mv ascension-launcher.AppImage ascension-launcher.AppImage_old\n\n      wget -O ascension-launcher.AppImage --progress=dot \"https://api.ascension.gg/api/bootstrap/launcher/latest?unix\"\n      2>&1 | sed ''s/^.*\\s\\+\\(.\\+\\)%\\s\\+\\(.\\+\\)\\s\\+\\(.\\+\\)\\s*$/\\1\\n# Downloading at\n      \\2\\/s ETA \\3/'' | zenity --title=\"Update Launcher\" --width=300 --progress --percentage=0\n      --no-cancel --auto-close\n\n      chmod u+x ascension-launcher.AppImage\n\n      fi\n\n      ./ascension-launcher.AppImage'\n    description: Create update launcher script\n    file: $GAMEDIR/updateAndRun.sh\n- chmodx: $GAMEDIR/updateAndRun.sh\n- execute:\n    command: echo \"version=$(curl https://api.ascension.gg/api/bootstrap/launcher\n      | grep -oP '\"version\":\"\\K.+?(?=\")')\" > $GAMEDIR/currentLauncherVersion\n    description: Fetch current launcher version\n- task:\n    arch: win32\n    description: Create 32bit Wine prefix\n    name: wine.create_prefix\n    prefix: $GAMEDIR/WoW\n- task:\n    app: remove_mono\n    arch: win32\n    description: Remove preinstalled mono in Wine prefix\n    name: wine.winetricks\n    prefix: $GAMEDIR/WoW\n- task:\n    arch: win32\n    description: Install wine mono\n    executable: mono\n    name: wine.wineexec\n    prefix: $GAMEDIR/WoW\n- task:\n    app: -f win10 ie8 corefonts dotnet48 vcrun2015\n    arch: win32\n    description: Install win10, ie8, corefonts, dotnet48 und vcrun2015 with winetricks\n    name: wine.winetricks\n    prefix: $GAMEDIR/WoW\n- input_menu:\n    description: Do you want to install DXVK?\n    id: dxvk\n    options:\n    - dxvk: 'Yes'\n    - good: 'No'\n    preselect: good\n- task:\n    app: $INPUT_dxvk\n    arch: win32\n    description: Install DXVK with winetricks\n    name: wine.winetricks\n    prefix: $GAMEDIR/WoW\nrequire-binaries: wine, winetricks, grep, wget, zenity, mono | mono-complete\n",
  "discord_id": ""
}